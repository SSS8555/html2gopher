#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
    no warnings 'uninitialized';
    use strict;
    use Text_Format;

    package abzac;
#------------------------------------------------------
    use vars qw( $data @links @images $linkSt $linkURL $abzac $justify $center $tag $pre);

#------------------------------------------------------
# null
#------------------------------------------------------
    sub null {
        my ($par)=@_;


        return $par;
    }
#------------------------------------------------------
# initNew
#------------------------------------------------------
    sub initNew {
        my ($abz,$cente,$justif)=@_;

        $data="";
        @links=();
        @images=();
        $abzac=$abz;
        $justify=$justif;
        $center=$cente;

        $pre=0;

        return;
    }
#------------------------------------------------------
# addText
#------------------------------------------------------
    sub addText {
        my ($text)=@_;

        $data.=$text;

        return;
    }
#------------------------------------------------------
# linkStart
#------------------------------------------------------
    sub linkStart {

        $linkSt=length($data);
        $linkURL="";

#        $data.="[";

        return;
    }
#------------------------------------------------------
# linkUrl
#------------------------------------------------------
    sub linkUrl {
        my ($url)=@_;

        $linkURL=$url;

        return;
    }
#------------------------------------------------------
# linkEnd
#------------------------------------------------------
    sub linkEnd {

        return unless $linkURL;

        return if $linkURL =~ m{^javascript:};

        return if $linkURL =~ m{^#};

        if ($linkURL eq substr($data,$linkSt))
        {
            substr($data,$linkSt)="";
        }
        $data.="[".(@links+1)."]";
        push(@links,[$linkSt,length($data),$linkURL]);


        return;
    }
#------------------------------------------------------
# addImg
#------------------------------------------------------
    sub addImg {
        my ($url)=@_;

        push(@images,[length($data),length($data),$url]);
        $data.="[IMG".(@images)."]";


        return;
    }
#------------------------------------------------------
# formPrint
#------------------------------------------------------
    sub formPrint {
        my ($fh_out,$cols)=@_;

        unless ($data)
        {
            print $fh_out "\n" if $tag ~~ [qw( br p )];
            return;
        }
        return if $tag ~~ [qw( style script)];

#        print $fh_out "~" x $abzac, $data, "\n";
        my $text = Text_Format->new (
            {
                text           =>  [], # all
                columns        =>  $cols, # format, paragraphs, center
                leftMargin     =>   0, # format, paragraphs, center
                rightMargin    =>   0, # format, paragraphs, center
                firstIndent    =>   $abzac, # format, paragraphs
                bodyIndent     =>   0, # format, paragraphs
                rightFill      =>   0, # format, paragraphs
                rightAlign     =>   $center == 2 ? 1 : 0, # format, paragraphs
                justify        =>   $justify, # format, paragraphs
                extraSpace     =>   0, # format, paragraphs
                abbrevs        =>  {}, # format, paragraphs
                hangingIndent  =>   0, # format, paragraphs
                hangingText    =>  [], # format, paragraphs
                noBreak        =>   0, # format, paragraphs
                noBreakRegex   =>  {}, # format, paragraphs
                tabstop        =>   8, # expand, unexpand,  center
            }
        ); # these are the default values
        if ($pre)
        {
           chomp($data);
           print $fh_out $data,"\n";
        }
        elsif ($center == 1)
        {
           print $fh_out $text->center($data);
        }
        else
        {
           print $fh_out $text->format($data);
        }

        for(my $i=1; $i<=@links; $i++)
        {
            my ($linkSt,$linkEND,$linkURL)=@{$links[$i-1]};
            print $fh_out "[$i] $linkURL\n";
        }
        for(my $i=1; $i<=@images; $i++)
        {
            my ($linkSt,$linkEND,$linkURL)=@{$images[$i-1]};
            print $fh_out "[IMG$i] $linkURL\n";
        }

        return;
    }
#------------------------------------------------------
1;

