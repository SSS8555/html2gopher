#!/usr/bin/perl -w -I.
#------------------------------------------------------
=head1 NAME

=head1 SYNOPSIS

=cut
#------------------------------------------------------
no warnings 'uninitialized';
use strict;
use HTML::Entities;
use HTML::TokeParser::Simple;
#use Encode;

use abzac;

#------------------------------------------------------
# null
#------------------------------------------------------
sub null {
    my ($par)=@_;

    return $par;
}
#------------------------------------------------------
# main
#------------------------------------------------------
sub main
{
     my ($fileName,$abzac,$cols)=@_;

     $abzac ||=5;
     $cols ||=80;


#     my @list = Encode->encodings(":all");print join(" ",@list);

     my $p = HTML::TokeParser::Simple->new(
       $fileName,
     ) or die qq{cant parse HTML};

#     open my $fh_out, q{>}, q{C:\prg_perl\gopher\server_saveV2\data\html\out.gopher}
#       or die qq{cant open file to write};
     open my $fh_out, q{>}, q{out.gopher}
       or die qq{cant open file to write};

     my $pre=0;

     abzac::initNew();

     while (my $t = $p->get_token)
     {
       
         if ($t->get_tag() =~ m{h\d+} and $t->is_start_tag()){
             abzac::formPrint($fh_out,$cols);
             abzac::initNew(0,1,0);
#             abzac::addText("[".$t->get_tag()."]");
         }
         elsif ($t->get_tag() =~ m{h\d+} and $t->is_end_tag()){
             abzac::formPrint($fh_out,$cols);
             abzac::initNew(0,0,1);
         }
         elsif ($t->is_start_tag(q{title})){
             abzac::formPrint($fh_out,$cols);
             abzac::initNew(0,2,0);
         }
         elsif ($t->is_end_tag(q{title})){
             abzac::formPrint($fh_out,$cols);
             abzac::initNew(0,0,1);
         }
         elsif ($t->is_start_tag(q{p})){
             abzac::formPrint($fh_out,$cols);
             abzac::initNew($abzac,0,1);
         }
         elsif ($t->is_end_tag(q{p}) or $t->is_tag(q{br})){
             abzac::formPrint($fh_out,$cols);
             abzac::initNew(0,0,1);
         }
         elsif ($t->get_tag() eq "hr" ){
             abzac::formPrint($fh_out,$cols);
             print $fh_out "-" x $cols , "\n";
             abzac::initNew(0,0,1);
         }
         elsif ($t->get_tag() ~~ [qw( li lu div script style pre tr)] ){
             abzac::formPrint($fh_out,$cols);
             abzac::initNew(0,0,1);
         }
         elsif ($t->is_start_tag(q{a})){
             abzac::linkStart();
             abzac::linkUrl($t->get_attr("href"));
         }
         elsif ($t->is_end_tag(q{a})){
             abzac::linkEnd();
         }
         elsif ($t->is_tag(q{img})){
             abzac::addImg($t->get_attr("src"));
         }
         elsif ($t->is_text){
             my $out = $t->as_is;
             if ($pre)
             {
                 $abzac::pre=1;
             }
             else
             {
                 for ($out){
        #           s/^\s+//;
        #           s/\s+$//;
    #               s/^[\n\r]+//;
    #               s/[\n\r]+$//;
                   s/[\n\r]+/ /g;
                 }
             }
             next unless $out;
             abzac::addText(decode_entities($out));
         }

         $abzac::tag=$t->get_tag() unless $t->is_text;
         if ($t->is_start_tag(q{pre})){
             $pre++;
         }
         elsif ($t->is_end_tag(q{pre})){
             $pre--;
         }
         elsif ($t->get_tag() ~~ [qw( div p )]){
             $pre=0;
         }
     }
     abzac::formPrint($fh_out,$cols);
     abzac::initNew();

     return;
}
#------------------------------------------------------
main(@ARGV);
